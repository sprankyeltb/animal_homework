﻿#include <iostream>
#include <string>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Default voice" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Wof Woof Woof " << std::endl;
	}
};

class Cat : public Dog
{
public:
	void Voice() override
	{
		std::cout << "Meow Mrrrr Meow" << std::endl;
	}
};

class Cow : public Cat
{
public:
	void Voice() override
	{
		std::cout << "Mooo Mooo" << std::endl;
	}
};

int main()
{
	//Создание объектов классов
	Dog a;
	Cat b;
	Cow c;

	//Создание указателей на объекты классов
	Dog* pa = &a;
	Cat* pb = &b;
	Cow* pc = &c;

	//Массив указателей на объекты классов
	Animal arr[]{ *pa,*pb,*pc };
	for (int i = 0; i < 1; i++)
	{
		pa->Voice();//Вызов 
		pb->Voice();//метода Voice()
		pc->Voice();//на каждом элементе массива
	}

	return 0;
}